<?php
//category.php
@include '../Conector.php';
$conex=new Conector();
$conex->selectBD();
include 'header.php';

//first select the category based on $_GET['cat_id']
$sql = "SELECT
			idCategoria,
			nombrecat,
			descripcioncat
		FROM
			categorias
		WHERE
			idCategoria = " . mysql_real_escape_string($_GET['id']);

$result = mysql_query($sql);

if(!$result)
{
	echo 'Disculpe el Tema no se puede mostrar, intente nuevamente .' . mysql_error();
}
else
{
	if(mysql_num_rows($result) == 0)
	{
		echo 'El Tema no existe aun no se ha creado.';
	}
	else
	{
		//display category data
		while($row = mysql_fetch_assoc($result))
		{
			echo '<h2>Tópicos de &prime;' . $row['nombrecat'] . '&prime; </h2><br />';
		}
	
		//do a query for the topics
		$sql = "SELECT	
					idTopico,
					titulo_topico,
					fecha_topico,
					topico_cat
				FROM
					topicos
				WHERE
					topico_cat = " . mysql_real_escape_string($_GET['id']);
		
		$result = mysql_query($sql);
		
		if(!$result)
		{
			echo 'Los Topicos no se pueden mostrar, vuelva a intentarlo.';
		}
		else
		{
			if(mysql_num_rows($result) == 0)
			{
				echo 'Aun no se han creado topicos para este tema.';
			}
			else
			{
				//prepare the table
				echo '<table border="1">
					  <tr>
						<th>Tópico</th>
						<th>Creado el</th>
					  </tr>';	
					
				while($row = mysql_fetch_assoc($result))
				{				
					echo '<tr>';
						echo '<td class="leftpart">';
							echo '<h3><a href="topic.php?id=' . $row['idTopico'] . '">' . $row['titulo_topico'] . '</a><br /><h3>';
						echo '</td>';
						echo '<td class="rightpart">';
							echo $row['fecha_topico'];
						echo '</td>';
					echo '</tr>';
				}
			}
		}
	}
}

include 'footer.php';
?>
