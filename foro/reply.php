<?php
@include '../Conector.php';
$conex=new Conector();
$conex->selectBD();
include 'header.php';

if($_SERVER['REQUEST_METHOD'] != 'POST')
{
	//someone is calling the file directly, which we don't want
	echo 'Error esta pagina no puede ser llamada directamente.';
}
else
{
	//check for sign in status
	if(!$_SESSION['signed_in'])
	{
		echo 'Debes estar registrado para poder escribir en este Foro.';
	}
	else
	{
		//a real user posted a real reply
		$sql = "INSERT INTO 
					posts(    contenido_post,
						  fecha_post,
						  post_topic,
						  post_grupo_user)
				VALUES ('" . $_POST['reply-content'] . "',
						NOW(),
						" . mysql_real_escape_string($_GET['id']) . ",
						" . $_SESSION['id'] . ")";
						
		$result = mysql_query($sql);
						
		if(!$result)
		{
			echo 'Tu comentario no se ha podido guardar, vuelva a intentarlo.';
		}
		else
		{
			echo 'Gracias, tu comentario ha sido guardado... <a href="topic.php?id=' . htmlentities($_GET['id']) . '">Ver</a>.';
		}
	}
}

include 'footer.php';
?>