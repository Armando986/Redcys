<?php
@include '../Conector.php';
$conex=new Conector();
$conex->selectBD();
include 'header.php';

$sql = "SELECT
			idTopico,
			titulo_topico
		FROM
			topicos
		WHERE
			topicos.idTopico = " .$_GET['id']." ";
			
$result = mysql_query($sql);

if(!$result)
{
	echo 'El topico no se puede mostrar, vuelva a intentarlo.';
}
else
{
	if(mysql_num_rows($result) == 0)
	{
		echo 'Este Topico no Existe.';
	}
	else
	{
		while($row = mysql_fetch_assoc($result))
		{
			//display post data
			echo '<table class="topic" border="1">
					<tr>
						<th colspan="2">' . $row['titulo_topico'] . '</th>
					</tr>';
		
			//fetch the posts from the database
			$posts_sql = "SELECT
						posts.post_topic,
						posts.contenido_post,
						posts.fecha_post,
						posts.post_grupo_user,
						usuarios.idUsuario,			
                                                usuarios.nombre,
                                                usuarios.email
					FROM
						posts
					LEFT JOIN
						usuarios
					ON
						posts.post_grupo_user = usuarios.idUsuario
					WHERE
						posts.post_topic = " . mysql_real_escape_string($_GET['id']) ."
                                        ORDER BY
                                                fecha_post
                                        DESC";
						
			$posts_result = mysql_query($posts_sql);
			
			if(!$posts_result)
			{
				echo '<tr><td>Los Post no se pueden mostrar, vuelva a intentarlo.</tr></td></table>';
			}
			else
			{
			
				while($posts_row = mysql_fetch_assoc($posts_result))
				{
					echo '<tr class="topic-post">
							<td class="user-post"><p id="emailtexto">' . $posts_row['nombre'] . '</p><br/><center><a href=mailto:'.$posts_row['email'].'>' .$posts_row['email'].'</a><br> '.date('d-m-Y H:i', strtotime($posts_row['fecha_post'])) . '</td>
							<td class="post-content"><textarea class="post"  name="" rows="100" cols="150">' . stripslashes($posts_row['contenido_post']) . '</textarea></td>
						  </tr>';
				}
			}
			
			if(!$_SESSION['signed_in'])
			{
				echo '<tr><td colspan=2>Debes Logearte  <a href="signin.php">Identificar</a>a Postear.  <a href="signup.php">sign up</a>.';
			}
			else
			{
				//show reply box
				echo '<tr><td colspan="2"><center><h2>Postear:</h2><br />
					<form method="post" action="reply.php?id=' . $row['idTopico'] . '">
						<textarea class="texto" name="reply-content"></textarea><br /><br />
						<input type="submit" value="Postear" />
					</td></tr></div>';
			}
			
			//finish the table
			echo '</table>';
		}
	}
}

include 'footer.php';
?>