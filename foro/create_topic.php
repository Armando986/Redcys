<?php
//create_topic.php
@include '../Conector.php';
$conex=new Conector();
$conex->selectBD();
include 'header.php';

echo '<center><h2>Crear Tópico</h2>';
if(isset($_SESSION['signed_in']) == false)
{
	//the user is not signed in
	echo 'Disculpa, debes estar registrado para poder acceder al foro...';
}
else
{
	//the user is signed in
	if($_SERVER['REQUEST_METHOD'] != 'POST')
	{	
		//the form hasn't been posted yet, display it
		//retrieve the categories from the database for use in the dropdown
		$sql = "SELECT
					idCategoria,
					nombrecat,
					descripcioncat
				FROM
					categorias
                                WHERE
                                        idGrupo='".$_SESSION['grupo']."'";
		
		$result = mysql_query($sql);
		
		if(!$result)
		{
			//the query failed, uh-oh :-(
			echo 'Error en la Conexion con la Base de Datos, intente mas tarde.';
		}
		else
		{
			if(mysql_num_rows($result) == 0)
			{
				//there are no categories, so a topic can't be posted
				if($_SESSION['idNivel'] == 2)
				{
					echo 'Todavia no se ha creado el tema para iniciar el Foro.';

				}
				else
				{
					echo 'Disculpa debes esperar a que el Administrador del foro inicie un tema.';
				}
			}
			else
			{
		
				echo '<form method="post" action="">
					<center>Titulo:<br> <input size="35" type="text" name="topic_subject" /><br />
					<center>Tema:';
				
				echo '<center><select name="topic_cat">';
					while($row = mysql_fetch_assoc($result))
					{
						echo '<option value="' . $row['idCategoria'] . '">' . $row['nombrecat'] . '</option>';
					}
				echo '</select><br />';	
					
				echo '<center>Mensaje: <br /><textarea rows="6" cols="35" name="post_content" /></textarea><br /><br />
					<center><input type="submit" value="Crear Tópico" />
				 </form>';
			}
		}
	}
	else
	{
		//start the transaction
		$query  = "BEGIN WORK;";
		$result = mysql_query($query);
		
		if(!$result)
		{
			//Damn! the query failed, quit
			echo 'Ha Ocurrido un mientras al momento de crear el topico, por favor intente nuevamente.';
		}
		else
		{
	
			//the form has been posted, so save it
			//insert the topic into the topics table first, then we'll save the post into the posts table
			$sql = "INSERT INTO 
						topicos(   titulo_topico,
							   fecha_topico,
							   topico_cat,
							   topico_grupo_user)
				   VALUES('" . $_POST['topic_subject']. "',
							   NOW(),
							   " . $_POST['topic_cat'] . ",
							   " . $_SESSION['id'] . "
							   )";
					 
			$result = mysql_query($sql);
			if(!$result)
			{
				//something went wrong, display the error
				echo 'Ha Ocurrido un Error mientras se registraba su topico, por favor vuelva a intentarlo.<br /><br />' . mysql_error();
				$sql = "ROLLBACK;";
				$result = mysql_query($sql);
			}
			else
			{
				//the first query worked, now start the second, posts query
				//retrieve the id of the freshly created topic for usage in the posts query
				$topicid = mysql_insert_id();
				
				$sql = "INSERT INTO
							posts(    contenido_post,
								  fecha_post,
								  post_topic,
								  post_grupo_user)
						VALUES
							('" . $_POST['post_content'] . "',
								  NOW(),
								  " . $topicid . ",
								  " . $_SESSION['id'] . "
							)";
				$result = mysql_query($sql);
				
				if(!$result)
				{
					//something went wrong, display the error
					echo 'Ha Ocurrido un error al momento se realizar su post, vuelva a intentarlo.<br /><br />' . mysql_error();
					$sql = "ROLLBACK;";
					$result = mysql_query($sql);
				}
				else
				{
					$sql = "COMMIT;";
					$result = mysql_query($sql);
					
					//after a lot of work, the query succeeded!
					echo 'Tu has creado tu <a href="topic.php?id='. $topicid . '">nuevo topico</a>.';
				}
			}
		}
	}
}

include 'footer.php';
?>
