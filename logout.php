<?php
session_start();
// Destroy sessions and cookies

    // Note: This will destroy the sessions, and not just the session variable data that the sessions hold
    session_destroy();

    setcookie("usEmail","x",time()-3600);
    setcookie("usPass","x",time()-3600);
    setcookie("usNivel","x",time()-3600);
    setcookie("usCheck","x",time()-3600);
     setcookie("usId","x",time()-3600);
    setcookie("usNombre","x",time()-3600);
    
    $_SESSION['signed_in'] = NULL;
    $_SESSION['grupo'] = NULL;
    header('Location: index.php');

    exit;

?>