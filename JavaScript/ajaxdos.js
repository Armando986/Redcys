

function objetoAjax(){


var xmlhttp=false;

try {
xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
}catch (e) {
try {
xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
} catch (E) {
xmlhttp = false;
}
}
if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
  xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
  }

function actualizar(form1){
    descripcion=document.form1.descripcion.value;
    idUsuario=document.form1.idUsuario.value;
    actestado=document.form1.actestado.value;


     if(idUsuario=="" || descripcion=="" ){
      jAlert("Introduzca escriba algo en su Estado...","Informacion");
     }
     else{
      //instanciamos el objetoAjax
      ajax=objetoAjax();

     //uso del medotod POST
      //archivo que realizará la operacion
      //registro.php
      ajax.open("POST","Procesos.php",true);

      ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
             //mostrar resultados en esta capa
            // divResultado.innerHTML = ajax.responseText;

             jAlert(ajax.responseText,"Información");
             limpiarEstado();

        }

      }
      ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      //enviando los valores"
      ajax.send("idUsuario="+escape(idUsuario)+"&descripcion="+escape(descripcion)+"&actestado="+escape(actestado));
     }


}

function limpiarEstado(){
   document.form1.descripcion.value="";
    document.form1.idUsuario.value="";

}

/*modificar Libro*/
function ModLibro(form1){
    titulo=document.form1.titulo.value;
    descripcion=document.form1.descripcion.value;
    autor=document.form1.autor.value;
    materia=document.form1.materias.value;
    grupo=document.form1.grupos.value;
    modlibro=document.form1.modlibro.value;
   // archivo=document.form1.archivo.value;
    idprof=document.form1.idprof.value;
    idLibros=document.form1.idLibros.value;

     if(titulo=="" || descripcion=="" || autor=="" || materia==""){
      jAlert("Introduzca los Datos para poder Modificar el Libro...","Error");
     }
     else{
      //instanciamos el objetoAjax
      ajax=objetoAjax();

     //uso del medotod POST
      //archivo que realizará la operacion
      //registro.php
      ajax.open("POST","Procesos.php",true);

      ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
             //mostrar resultados en esta capa
            // divResultado.innerHTML = ajax.responseText;

              jAlert(ajax.responseText,"Información");
             limpiarformlibro();

        }

      }
      ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      //enviando los valores"
      ajax.send("titulo="+escape(titulo)+"&descripcion="+escape(descripcion)+"&idMateria="+escape(materia)
                +"&autor="+escape(autor)+"&modlibro="+escape(modlibro)
                +"&idGrupo="+escape(grupo)+"&idUsuario="+escape(idprof)+"&idLibros="+escape(idLibros));
     }

}

/*funcion limpiar*/

function limpiarformlibro(){
   document.form1.materias.selectedIndex=1;
    document.form1.grupos.selectedIndex=1;
 document.form1.titulo.value="";
    document.form1.descripcion.value="";
   document.form1.autor.value="";
 

 
}

//modificacion de grupo
function ModGrupo(form1){
     nombre=document.form1.nombre.value;
     objetivos=document.form1.objetivos.value;
     materia=document.form1.materia.value;
     profesor=document.form1.profesor.value;
     modgrupo=document.form1.modgrupo.value;
     idgrupo=document.form1.idGrupo.value;
    

     if(nombre=="" || objetivos=="" || materia=="" || profesor==""){
      jAlert("Introduzca los Datos para poder Registrar el Grupo...","Error");
     }
     else{
      //instanciamos el objetoAjax
      ajax=objetoAjax();

     //uso del medotod POST
      //archivo que realizará la operacion
      //registro.php
      ajax.open("POST","Procesos.php",true);

      ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
             //mostrar resultados en esta capa
            // divResultado.innerHTML = ajax.responseText;

            jAlert(ajax.responseText,"Información");
             limpiargrupo();


        }

      }
      ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      //enviando los valores"
      ajax.send("nombre="+escape(nombre)+"&objetivos="+escape(objetivos)+"&idMateria="+escape(materia)
                +"&idProfesor="+escape(profesor)+"&modgrupo="+escape(modgrupo)+"&idGrupo="+escape(idgrupo));
     }
}

/*CREAR PUBLICACION*/
function CrearLibro(form1){
     grupos=document.form1.grupos.value;
     nombre=document.form1.nombre.value;
     materia=document.form1.materia.value;
     descripcion=document.form1.descripcion.value;
     autor=document.form1.autor.value;
     libro=document.form1.libro.value;
     crearlibro=document.form1.crearlibro.value;

     if(nombre=="" || autor==""){
      jAlert("Introduzca los Datos Obligatorios para poder crear la Publicacion...","Error");
     }
     else{
      //instanciamos el objetoAjax
      ajax=objetoAjax();

     //uso del medotod POST
      //archivo que realizará la operacion
      //registro.php
      ajax.open("POST","Procesos.php",true);

      ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
             //mostrar resultados en esta capa
            // divResultado.innerHTML = ajax.responseText;

              jAlert(ajax.responseText,"Información");
             limpiargrupo();


        }

      }
      ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      //enviando los valores"
      ajax.send("idGrupo="+escape(grupos)+"&nombre="+escape(nombre)+"&idMateria="+escape(materia)
                +"&descripcion="+escape(descripcion)+"&libro="+escape(libro)+"&autor="+escape(autor)+"&crearlibro="+escape(crearlibro));
     }
}


//registro de grupo
function Grupo(form1){
     nombre=document.form1.nombre.value;
     objetivos=document.form1.objetivos.value;
     materia=document.form1.materia.value;
     profesor=document.form1.profesor.value;
     regrupo=document.form1.regrupo.value;

    

     if(nombre=="" || objetivos=="" || materia=="" || profesor==""){
      jAlert("Introduzca los Datos para poder Registrar el Grupo...","Error");
     }
     else{
      //instanciamos el objetoAjax
      ajax=objetoAjax();

     //uso del medotod POST
      //archivo que realizará la operacion
      //registro.php
      ajax.open("POST","Procesos.php",true);

      ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
             //mostrar resultados en esta capa
            // divResultado.innerHTML = ajax.responseText;

              jAlert(ajax.responseText,"Información");
             limpiargrupo();


        }

      }
      ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      //enviando los valores"
      ajax.send("nombre="+escape(nombre)+"&objetivos="+escape(objetivos)+"&idMateria="+escape(materia)
                +"&idProfesor="+escape(profesor)+"&regrupo="+escape(regrupo));
     }
}

/*limpiar grupo*/
function limpiargrupo(){
      document.form1.materia.selectedIndex=0;
      document.form1.profesor.selectedIndex=0;
      document.form1.nombre.value="";
       document.form1.objetivos.value="";
}



//registro de materia
function RegMat(form1){
    nombre=document.form1.nombre.value;
    codigo=document.form1.codigo.value;
    descripcion=document.form1.descripcion.value;
    credito=document.form1.creditos.value;
    idcarrera=document.form1.carrera.value;
    regmat=document.form1.regmat.value;

     if(nombre=="" || codigo=="" || descripcion=="" || credito==0){
      jAlert("Introduzca los Datos de la Asignatura...","Error");
     }
    else{
    //instanciamos el objetoAjax
      ajax=objetoAjax();

     //uso del medotod POST
      //archivo que realizará la operacion
      //registro.php
      ajax.open("POST","Procesos.php",true);

      ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
             //mostrar resultados en esta capa
            // divResultado.innerHTML = ajax.responseText;

              jAlert(ajax.responseText,"Información");
             limpiarmat();


        }

      }
      ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      //enviando los valores"
      ajax.send("nombre="+escape(nombre)+"&codigo="+escape(codigo)+"&descripcion="+escape(descripcion)
                +"&credito="+escape(credito)+"&idcarrera="+escape(idcarrera)+"&regmat="+escape(regmat));

}}

function limpiarmat(){
    document.form1.nombre.value="";
    document.form1.codigo.value="";
    document.form1.descripcion.value="";
    document.form1.creditos.selectedIndex=0;
    document.form1.carrera.selectedIndex=0;
    document.form1.regmat.value="";
}


//modificar dats de Materia
////registro de materia
function ModMat(form1){
    nombre=document.form1.nombre.value;
    codigo=document.form1.codigo.value;
    descripcion=document.form1.descripcion.value;
    credito=document.form1.creditos.value;
    idcarrera=document.form1.carrera.value;
    modmat=document.form1.modmat.value;

     if(nombre=="" || codigo=="" || descripcion=="" || credito==0){
      jAlert("Introduzca los Datos de la Asignatura...","Error");
     }
    else{
    //instanciamos el objetoAjax
      ajax=objetoAjax();

     //uso del medotod POST
      //archivo que realizará la operacion
      //registro.php
      ajax.open("POST","Procesos.php",true);

      ajax.onreadystatechange=function() {
        if (ajax.readyState==4) {
             //mostrar resultados en esta capa
            // divResultado.innerHTML = ajax.responseText;

              jAlert(ajax.responseText,"Información");
             limpiarmatedmod();


        }

      }
      ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
      //enviando los valores"
      ajax.send("nombre="+escape(nombre)+"&codigo="+escape(codigo)+"&descripcion="+escape(descripcion)
                +"&credito="+escape(credito)+"&idcarrera="+escape(idcarrera)+"&modmat="+escape(modmat));

}

}

/*limpiar mate mod*/
function limpiarmatemod(){
  document.form1.nombre.value="";
    document.form1.codigo.value="";
    document.form1.descripcion.value="";
    document.form1.creditos.selectedIndex=0;
   document.form1.carrera.selectedIndex=0;
   
}

//registrar datos Esutidnate
function enviar(form1){
  //donde se mostrará lo resultados
  divResultado = document.getElementById('mensaje');
  //valores de los inputs

  nom=document.form1.nombre.value;

  apell=document.form1.apellido.value;
 
  edad=document.form1.edad.value;

  sex=document.form1.sexo.value;

  fecha=document.form1.fecha.value;

  ced=document.form1.cedula.value;

  carrera=document.form1.carrera.value;

  tel=document.form1.telefono.value;

  email=document.form1.email.value;

  pass=document.form1.password.value;

  web=document.form1.web.value;

  regestudiante=document.form1.regestudiante.value;
 
  if(valEmail(email) || email=="" || pass=="" || nom=="" || apell=="" ){
     jAlert('Introduzca sus datos, por favor', 'Error');

  }
else{
  //instanciamos el objetoAjax
  ajax=objetoAjax();

 //uso del medotod POST
  //archivo que realizará la operacion
  //registro.php
  ajax.open("POST","Procesos.php",true);

  ajax.onreadystatechange=function() {
    if (ajax.readyState==4) {
         //mostrar resultados en esta capa
        // divResultado.innerHTML = ajax.responseText;

         jAlert(ajax.responseText,"Información");
         limpiarest();
                
     
    }
  
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores"
  ajax.send("nombre="+escape(nom)+"&apellido="+escape(apell)+"&cedula="+escape(ced)
            +"&sexo="+escape(sex)+"&carrera="+escape(carrera)+"&telefono="+escape(tel)+"&email="+escape(email)
            +"&password="+escape(pass)+"&web="+escape(web)+"&edad="+escape(edad)+"&fecha="+escape(fecha)
            +"&regestudiante="+escape(regestudiante));
  
}
  }

  /*limpiar campos est*/
  function limpiarest(){
  document.form1.nombre.value="";

  document.form1.apellido.value="";

  document.form1.edad.selectedIndex=0;

  document.form1.sexo.selectedIndex=0;

  document.form1.fecha.value="";

  document.form1.cedula.value="";

  document.form1.carrera.selectedIndex=0;

  document.form1.telefono.value="";

  document.form1.email.value="";

  document.form1.password.value="";

  document.form1.web.value="";

  }

  //modificar datos de estudiante
  function ModDatosEstu(form1){
          nom=document.form1.nombre.value;

          apell=document.form1.apellido.value;

          edad=document.form1.edad.value;

          sex=document.form1.sexo.value;

          fecha=document.form1.fecha.value;

          ced=document.form1.cedula.value;

          carrera=document.form1.carrera.value;

          tel=document.form1.telefono.value;

          email=document.form1.email.value;

          pass=document.form1.password.value;

          web=document.form1.web.value;

          modestudiante=document.form1.modestudiante.value;
  }

  //registrar profesor y personal administrativo
  function enviar2(form1){
   //valores del formulario
      nom=document.form1.nombre.value;

      apell=document.form1.apellido.value;

      edad=document.form1.edad.value;

      sex=document.form1.sexo.value;

      fecha=document.form1.fecha.value;

      ced=document.form1.cedula.value;

      carrera=document.form1.carrera.value;

      tel=document.form1.telefono.value;

      email=document.form1.email.value;

      pass=document.form1.password.value;

      web=document.form1.web.value;

      profes=document.form1.profesion.value;

      nivel=document.form1.nivel.value;

      regprofadm=document.form1.regprofadm.value;


   

        if(email=="" || pass=="" || carrera=="" || nom=="" || apell=="" || nivel=="Seleccione su Nivel" || valEmail(email) ){
              jAlert("Introduzca sus Datos o Verifique, por favor","Error");
          }
        else{

           

          //instanciamos el objetoAjax
          ajax=objetoAjax();

         //uso del medotod POST
          //archivo que realizará la operacion
          //registro.php
          ajax.open("POST","Procesos.php",true);

          ajax.onreadystatechange=function() {
         if (ajax.readyState==4) {
                 //mostrar resultados en esta capa
                // divResultado.innerHTML = ajax.responseText;

                  jAlert(ajax.responseText,"Información");
                 limpieza1();


            }

          }
          ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
          //enviando los valores"
          ajax.send("nombre="+escape(nom)+"&apellido="+escape(apell)+"&cedula="+escape(ced)
                    +"&sexo="+escape(sex)+"&carrera="+escape(carrera)+"&telefono="+escape(tel)+"&email="+escape(email)
                    +"&password="+escape(pass)+"&web="+escape(web)+"&edad="+escape(edad)+"&fecha="+escape(fecha)
                    +"&profesion="+escape(profes)+"&nivel="+escape(nivel)+"&regprofadm="+escape(regprofadm));

        }


  }

  /*limpieza de campo*/
  function limpieza1(){
       document.form1.nombre.value="";

      document.form1.apellido.value="";

     document.form1.edad.selectedIndex=0;

      document.form1.sexo.selectedIndex=0;

      document.form1.fecha.value="";

     document.form1.cedula.value="";

      document.form1.carrera.selectedIndex=0;

      document.form1.telefono.value="";

      document.form1.email.value="";

      document.form1.password.value="";

      document.form1.web.value="";

      document.form1.profesion.value="";

      document.form1.nivel.selectedIndex=0;

  }

// modificar datos prof y adm
function ModDatos(form1){

  nom=document.form1.nombre.value;

  apell=document.form1.apellido.value;

  edad=document.form1.edad.value;

  sex=document.form1.sexo.value;

  fecha=document.form1.fecha.value;

  ced=document.form1.cedula.value;

  carrera=document.form1.carrera.value;

  tel=document.form1.telefono.value;

  email=document.form1.email.value;

  pass=document.form1.password.value;

  web=document.form1.web.value;

  nivel=document.form1.nivel.value;

  profes=document.form1.profesion.value;

  modprofadm=document.form1.modprofadm.value;

 if(  valEmail(email) || email=="" || pass=="" || nom=="" || apell=="" ){
      jAlert("Introduzca sus Datos...","Error");
  }
 else{
  //instanciamos el objetoAjax
  ajax=objetoAjax();

 //uso del medotod POST
  //archivo que realizará la operacion
  //registro.php
  ajax.open("POST","Procesos.php",true);

  ajax.onreadystatechange=function() {
    if (ajax.readyState==4) {
         //mostrar resultados en esta capa
        // divResultado.innerHTML = ajax.responseText;

          jAlert(ajax.responseText,"Información");
         limpiar();


    }

  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores"
  ajax.send("nombre="+escape(nom)+"&apellido="+escape(apell)+"&cedula="+escape(ced)
            +"&sexo="+escape(sex)+"&carrera="+escape(carrera)+"&telefono="+escape(tel)+"&email="+escape(email)
            +"&password="+escape(pass)+"&web="+escape(web)+"&edad="+escape(edad)+"&fecha="+escape(fecha)
            +"&profesion="+escape(profes)+"&modprofadm="+escape(modprofadm));

}

}

function olvido(form){
 email=document.form.email.value;

    if(valEmail(email)){
                    jAlert("Email no es valido","Error");
    }
    else if(email!=""){
    
         jAlert("Se Envio a su Correo su password","Informacion");
    }
   
}

function inicio(form){
    email=form.email.value;
    clave=form.clave.value;

            if(email=="" && clave=="" ){
                 jAlert("Introduzca sus datos para el inicio de sesion","Error");
                 return false;
            }
            else if(email==""){
                 jAlert("Introduzca su Email...","Error");
                 return false;

            }
            else if(clave==""){
                 jAlert("Introduzca sus clave para el inicio de sesion","Error");
                 return false;
            }      
            else if(valEmail(email)){
                    jAlert("Email no es valido","Error");
                    return false;
            }
          else{


                    form.submit();
                    return true;
             }
   
}

 function valEmail(valor){    // Cortesía de http://www.ejemplode.com
             re=/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/
                 if(!re.exec(valor))    {
                          return true;
                 }else{
                          return false;
                  }
          }

function limpiar(){
 document.form1.profesion.value="";
   document.form1.nivel.selectedIndex=0;
document.form1.nombre.value="";
  document.form1.apellido.value="";
  document.form1.edad.selectedIndex=0;
  document.form1.fecha.value="";
  document.form1.cedula.value="";
  document.form1.carrera.selectedIndex=0;
  document.form1.telefono.value="";
  document.form1.email.value="";
  document.form1.password.value="";
  document.form1.web.value="";
  document.form1.nombre.focus();
  document.form1.sexo.selectedIndex=0;
  document.form1.codigo.value="";
  document.form1.descripcion.value="";
  document.form1.creditos.selectedIndex=0;
  document.form1.nombre.value="";
  document.form1.codigo.value="";


  document.form1.creditos.selectedIndex=0;
  document.form1.materia.selectedIndex=0;
  document.form1.profesor.selectedIndex=0;
  document.form1.carrera.selectedIndex=0;
}


/*modificar la noticia*/
function mod_noticia(fnoticia){

  titulo=document.fnoticia.titulo.value;
  noticia=document.fnoticia.noticia.value;
  fecha=document.fnoticia.fecha.value;
  idNoticias=document.fnoticia.idNoticias.value;
  modnoticia=document.fnoticia.modnoticia.value;



  //instanciamos el objetoAjax
  ajax=objetoAjax();
  //uso del medotod POST
  //archivo que realizará la operacion
  //registro.php
  ajax.open("POST","Procesos.php",true);

  ajax.onreadystatechange=function() {
    if (ajax.readyState==4) {
         //mostrar resultados en esta capa

         jAlert(ajax.responseText,"Información");
         //limpiar
         limpiar_noticia();
    }
    else{

        divformulario.style.display='none';
        divResultado.style.display='block';
        divResultado.innerHTML = "<img src='images/loading.gif'><br><br><font color='#666666'>Enviando<br>Espere un momento por favor...</font>";
    }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores"
  ajax.send("titulo="+escape(titulo)+"&noticia="+escape(noticia)+"&fecha="+escape(fecha)+"&idNoticias="+escape(idNoticias)
            +"&modnoticia="+escape(modnoticia));


  }

/*enviar la noticia*/
function enviar_noticia(){
  //donde se mostrará lo resultados
  divResultado = document.getElementById('mennoticia');
  divformulario=document.getElementById('fnoticia');

  forma=document.getElementById('fnoticia');
  //valores de los inputs
  titulo=document.fnoticia.titulo.value;
  noticia=document.fnoticia.noticia.value;
  fecha=document.fnoticia.fecha.value;
  //instanciamos el objetoAjax
  ajax=objetoAjax();
  //uso del medotod POST
  //archivo que realizará la operacion
  //registro.php
  ajax.open("POST", "noticias.php",true);

  ajax.onreadystatechange=function() {
    if (ajax.readyState==4) {
         //mostrar resultados en esta capa

         divResultado.innerHTML = ajax.responseText
         //limpiar
         limpiar_noticia();
    }
    else{

        divformulario.style.display='none';
        divResultado.style.display='block';
        divResultado.innerHTML = "<img src='images/loading.gif'><br><br><font color='#666666'>Enviando<br>Espere un momento por favor...</font>";
    }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores"
  ajax.send("titulo="+escape(titulo)+"&noticia="+escape(noticia)+"&fecha="+escape(fecha))


  }

function limpiar_noticia(){
  document.fnoticia.titulo.value="";
  document.fnoticia.noticia.value="";
  document.fnoticia.fecha.value="";

}

//consulta de noticias

function get_noticia(){
  //donde se mostrará lo resultados
  divResultado = document.getElementById('mennoticia');
  divformulario=document.getElementById('fnoticia');

  forma=document.getElementById('fnoticia');
  //valores de los inputs
  titulo=document.fnoticia.titulo.value;
  noticia=document.fnoticia.noticia.value;
  fecha=document.fnoticia.fecha.value;
  //instanciamos el objetoAjax
  ajax=objetoAjax();
  //uso del medotod POST
  //archivo que realizará la operacion
  //registro.php
  ajax.open("POST", "noticias.php",true);

  ajax.onreadystatechange=function() {
    if (ajax.readyState==4) {
         //mostrar resultados en esta capa

         divResultado.innerHTML = ajax.responseText
         //limpiar
         limpiar_noticia();
    }
    else{

        divformulario.style.display='none';
        divResultado.style.display='block';
        divResultado.innerHTML = "<img src='images/loading.gif'><br><br><font color='#666666'>Enviando<br>Espere un momento por favor...</font>";
    }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores"
  ajax.send("titulo="+escape(titulo)+"&noticia="+escape(noticia)+"&fecha="+escape(fecha))


  }

  function MostrarConsulta(datos){

        divForm=document.getElementById('connoti')
        divResultado = document.getElementById('resulnoti');

        ajax=objetoAjax();

        ajax.open("POST", datos);

        ajax.setRequestHeader("Content-Type",
        "application/x-www-form-urlencoded");
        
        ajax.onreadystatechange=function() {

               if (ajax.readyState==4) {


                       divResultado.innerHTML = ajax.responseText;
                       
               }
                  else{
                    divForm.style.display="none";
                    divForm.innerHTML = "<img src='images/loading.gif'><br><br><font color='#666666'>Enviando<br>Espere un momento por favor...</font>";
                }

        }

        ajax.send(null)

}
  
  function MostrarC2(datos){

        divForm=document.getElementById('connoti')
        divResultado = document.getElementById('resulnot');

        ajax=objetoAjax();

        ajax.open("POST", datos);

        ajax.setRequestHeader("Content-Type",
        "application/x-www-form-urlencoded");
        
        ajax.onreadystatechange=function() {

               if (ajax.readyState==4) {


                       divResultado.innerHTML = ajax.responseText;
                       
               }
                  else{
                    divForm.style.display="none";
                    divForm.innerHTML = "<img src='images/loading.gif'><br><br><font color='#666666'>Enviando<br>Espere un momento por favor...</font>";
                }

        }

        ajax.send(null)

}

   function MostrarC3(){   

         for (i = 0; i < document.fedit.elements.length; i++){
             if(document.fedit.elements[i].type=="checkbox")
                 if(document.fedit.elements[i].checked)
                  num=i;
            
        }

    
        radio=document.fedit.radio[num].value;
    
        divForm=document.getElementById('fo');
        divResultado = document.getElementById('resp');

        ajax=objetoAjax();

        ajax.open("POST", 'FormNotiEdit.php');

        ajax.setRequestHeader("Content-Type",
        "application/x-www-form-urlencoded");

        ajax.onreadystatechange=function() {

               if (ajax.readyState==4) {

                        divForm.style.display="none";
                       divResultado.innerHTML = ajax.responseText;

               }
                  else{
                    divForm.style.display="none";
                    divForm.innerHTML = "<img src='images/loading.gif'><br><br><font color='#666666'>Enviando<br>Espere un momento por favor...</font>";
                }

        }

        ajax.send("radio="+escape(radio));

}
  
function editar_noticia(){
  //donde se mostrará lo resultados
  divResultado = document.getElementById('mennoticia');
  divformulario=document.getElementById('fnoticia');

  forma=document.getElementById('fnoticia');
  //valores de los inputs
  id=document.fnoticia.id.value;
  titulo=document.fnoticia.titulo.value;
  noticia=document.fnoticia.noticia.value;
  fecha=document.fnoticia.fecha.value;
  //instanciamos el objetoAjax
  ajax=objetoAjax();
  //uso del medotod POST
  //archivo que realizará la operacion
  //registro.php
  ajax.open("POST", "editarnot.php",true);

  ajax.onreadystatechange=function() {
    if (ajax.readyState==4) {
         //mostrar resultados en esta capa

         divResultado.innerHTML = ajax.responseText
         //limpiar
         limpiar_noticia();
    }
    else{

        divformulario.style.display='none';
        divResultado.style.display='block';
        divResultado.innerHTML = "<img src='images/loading.gif'><br><br><font color='#666666'>Modificando Noticia <br>Espere un momento por favor...</font>";
    }
  }
  ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  //enviando los valores"
  ajax.send("id="+escape(id)+"&titulo="+escape(titulo)+"&noticia="+escape(noticia)+"&fecha="+escape(fecha))


  }

    function noticiaspublicas(datos){


        divResultado = document.getElementById('resulnoti');

        ajax=objetoAjax();

        ajax.open("POST", datos);

        ajax.setRequestHeader("Content-Type",
        "application/x-www-form-urlencoded");

        ajax.onreadystatechange=function() {

               if (ajax.readyState==4) {


                       divResultado.innerHTML = ajax.responseText;

               }
                  else{

                    divForm.innerHTML = "<img src='images/loading.gif'><br><br><font color='#666666'>Enviando<br>Espere un momento por favor...</font>";
                }

        }

        ajax.send(null)

}

  function MostrarC4(datos){

        divForm=document.getElementById('connoti')
        divResultado = document.getElementById('resulnot');

        ajax=objetoAjax();

        ajax.open("POST", datos);

        ajax.setRequestHeader("Content-Type",
        "application/x-www-form-urlencoded");

        ajax.onreadystatechange=function() {

               if (ajax.readyState==4) {


                       divResultado.innerHTML = ajax.responseText;

               }
                  else{
                    divForm.style.display="none";
                    divForm.innerHTML = "<img src='images/loading.gif'><br><br><font color='#666666'>Enviando<br>Espere un momento por favor...</font>";
                }

        }

        ajax.send(null)

}

   function MostrarC5(){

         for (i = 0; i < document.feli.elements.length; i++){
             if(document.feli.elements[i].type=="checkbox")
                 if(document.feli.elements[i].checked)
                  num=i;

        }


        radio=document.feli.radio[num].value;

        divForm=document.getElementById('fo');
        divResultado = document.getElementById('resp');

        ajax=objetoAjax();

        ajax.open("POST", 'Eliminarnoticia.php');

        ajax.setRequestHeader("Content-Type",
        "application/x-www-form-urlencoded");

        ajax.onreadystatechange=function() {

               if (ajax.readyState==4) {

                        divForm.style.display="none";
                       divResultado.innerHTML = ajax.responseText;

               }
                  else{
                    divForm.style.display="none";
                    divForm.innerHTML = "<img src='images/loading.gif'><br><br><font color='#666666'>Enviando<br>Espere un momento por favor...</font>";
                }

        }

        ajax.send("radio="+escape(radio));

}